# Dunelm Pairing Exercise

This repo contains the source for the Dunelm pairing exercise. Here's some useful information to get you started.

## Pre Requisites

- NodeJS 16 (https://nodejs.org/en/)
- Yarn 1 (https://classic.yarnpkg.com/en/docs/install)
- Git (https://git-scm.com/downloads)

## Description

1. This repo contains Dunelms product api.

- a node service written using [ExpressJS](https://expressjs.com/) and responsible for retrieving and returning product data

2. The architecture of this service is implemented as follows:

```
+---------------+        +---------------+
|               |        |               |
|               |        |               |
|     REST      +  http  +      api      +
|    Client     |------->|   (Express)   |
|               |        |               |
|               |        |               |
+---------------+        +---------------+
```

## Getting Started

- Run `yarn` to install all dependencies
- Run `yarn start` to run the service
  - This will spin up the api on `http://localhost:3002/api/products`
- Run `yarn test` to execute the tests

When you've got this up and running please move on to the [Exercise](docs/exercise.md).
