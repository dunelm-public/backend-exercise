import express from 'express'

const server = express()
  .get('/api/products', (req, res) => {
    res.json({ data: { message: 'Dunelm Products Service' } })
  })
  .listen(process.env.PORT, () => {
    // tslint:disable-next-line:no-console
    console.log('api listening', server.address())
  })
