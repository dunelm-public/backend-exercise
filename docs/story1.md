[Back to Exercise](./exercise.md)

### Story #1

```
Given the products api is up and running
When I make a request for all products
Then I must be presented with a list of all products
```

- A list of products can be found in `src/products.json`. This should be treated as the data source for the api.
- The response from the api must include the following properties for all products:
  - name
  - description
  - category
  - colour
  - price
  - inStock

If you complete this please move on to [Story 2](./story2.md).
