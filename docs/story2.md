[Back to Story 1](./exercise.md)

### Story #2

```
Given the products api is up and running
When I make a request for all products
And I choose to view buyable products only
Then I must be presented with a list of products available to purchase
```

- Buyable products are those which have their `inStock` property set to `true`.
